
const Task = require('./../models/Task.js');

module.exports.createTask = (reqBody) => {

	let newTask = new Task({
		name: reqBody.name
	})

	return newTask.save().then( result => result)
}

module.exports.getAllTasks = () => {

	return Task.find().then( (result) => {
		return result;
	})
}

module.exports.updateTask = (reqParams, reqBody) => {

	return Task.findByIdAndUpdate( reqParams.id, {status: reqBody.status}).then( result => result)
}