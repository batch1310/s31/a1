
//to query or mmanipulate database, import the model to use the model methods
const User = require('./../models/User.js');
	//find() ---> array of documents []
	//findOne()
	//findOneAnd..
	//findByIdAnd..

//userController is a module for business logic/ API tasks to be done
module.exports.getAllUsers = () => {
	//function has to do the task

	//first, find the matching document/s(single or array of documents)
		//manipulating database returns a promise
		//.then() handles the promise
			//resolve
			//reject
	return User.find().then( (result) => {
		// console.log(result);
		return result;
	})
	//return the matching document
}

module.exports.register = (reqBody) => {

	//find the matching nt using model method
	return User.findOne( {email: reqBody.email}).then( (result) => {
		console.log(result)

		//if result != null, return false/`User exist already`
		//if result == null, save the
		if(result != null){
			return `User already exists`
		} else {

			let newUser = new User({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				mobileNo: reqBody.mobileNo,
				password: reqBody.password
			})

			return newUser.save().then( (result) => {
				return `User ${reqBody.email} saved!`
			})
		}
	})
}

module.exports.updateUser = (reqBody) => {

	return User.findOneAndUpdate( {email: reqBody.email}, {isAdmin: true}, {new: true}).then( result => {
		return result
	})
}


module.exports.deleteUser = (reqBody) => {

	return User.findOneAndDelete( {email: reqBody.email}).then( result => {
		return true
	})
}


module.exports.getSpecificUser = (reqBody) => {

	return User.findOne( {email: reqBody.email}).then( result => {
		return result
	})
}

module.exports.getById = (params) => {

	return User.findById(params).then( result => {
		return result
	})
}
