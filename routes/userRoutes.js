
//object that handles client requests and send responses
//property of express
	//Router();

const express = require("express");
const router = express.Router();

//in order to use controllers module properties, import controllers module
const userController = require('./../controllers/userControllers.js');

//Retrieving array of documents from database using "GET" method and find() model method
router.get("/", (request, response) => {
	//check the request if there's data to be used
	//console.log(request)

	//invoke the function from controllers
	userController.getAllUsers().then( result => {
		response.send(result)
	})
})


//Add a user in the database using "POST" method http method and save() method
router.post("/add-user", (request, response) => {
	//check the request if there's data to be used
	// console.log(request.body);

	//invoke the function from controllers module
	userController.register(request.body).then( result => {
		response.send(result)
	})
})

//Update user informtaion
router.put("/update-user", (request, response) => {
	// console.log(request.body) //object

	userController.updateUser(request.body).then( result => {
		response.send(result)
	});
})


//Delete user
router.delete("/delete-user", (request, response) => {

	userController.deleteUser(request.body).then( result => {
		response.send(result)
	})
})

/*Retrieve user using two model methods
	findOne()
	findById()
*/

//findOne()
router.get("/specific-user", (request, response) => {

	userController.getSpecificUser(request.body).then( result => {
		response.send(result)
	})
})


//findById() 619655cce1389512e7be8f9f
router.get("/:id", (request, response) => {
	//request properties:
		//url or params
		//body
		//http methods
		//headers

	userController.getById(request.params.id).then( result => {
		response.send(result)
	})
})


//in order for the routes to be use in other modules we need to export it first
module.exports = router;