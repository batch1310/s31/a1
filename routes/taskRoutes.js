const express = require("express");
const router = express.Router();

const taskController = require('./../controllers/taskControllers.js');

router.post("/", (req, res) => {

	taskController.createTask(req.body).then( result => res.send(result));
})


router.put("/:id", (req, res) => {

	taskController.updateTask(req.params, req.body).then( result => res.send(result))
})


router.get("/", (req, res) => {
	taskController.getAllTasks().then( result => res.send(result))
})

module.exports = router;